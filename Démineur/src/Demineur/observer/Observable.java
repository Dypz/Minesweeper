package Demineur.observer;

import java.util.ArrayList;
import Demineur.model.Cell;


public interface Observable {
    
    public void addObserver(Observer obs);
    public void removeObserver();
    public void notifyObserver(ArrayList<ArrayList<Cell>> grid, boolean still_in_game, int nbMines);
    
}
