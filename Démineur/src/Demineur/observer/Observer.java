
package Demineur.observer;

import Demineur.model.Cell;
import java.util.ArrayList;


public interface Observer {
    public void update(ArrayList<ArrayList<Cell>> grid, boolean still_in_game,int nbMines);
}

