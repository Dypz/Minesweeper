/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Demineur.controller;
import Demineur.model.AbstractModel;
import Demineur.model.CellStates;

/**
 *
 * @author aubin
 */
public class MinesweeperController extends AbstractController{
    
    public MinesweeperController(AbstractModel minesweeper){
        super(minesweeper);
    }
    
    public void control(){
        // we notify the model of an action if the control is check
        if(grid_case_control[0] >= 0 && grid_case_control[1] >= 0) // indice case > 0
        {
            // check if the indices (i,j) are not out of the grid
            if(grid_case_control[0] < this.minesweeper.getHeight() && grid_case_control[1] < this.minesweeper.getWidth() )
            {
                switch(this.action)
                {
                    case "REVEAL":
                        this.minesweeper.revealCase(this.grid_case_control[0],this.grid_case_control[1]);
                        break;
                    case "MARK_AS_MINE":
                        this.minesweeper.markCase(this.grid_case_control[0],this.grid_case_control[1],CellStates.MARKMINE);
                        break;
                    case "MARK_AS_UNKNOWN":
                        this.minesweeper.markCase(this.grid_case_control[0],this.grid_case_control[1],CellStates.MARKSUSPECT);
                        break;
                    case "DELETE_MARK":
                        this.minesweeper.markCase(this.grid_case_control[0],this.grid_case_control[1],CellStates.HIDDEN);
                        break;
                    default:
                        // nothing if the action is not identified
                }
            }
        }
    }
    
}
