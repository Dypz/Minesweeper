package Demineur.controller;
import Demineur.model.AbstractModel;


public abstract class AbstractController {
    protected AbstractModel minesweeper;
    protected int[] grid_case_control = new int[2];
    protected String action; 
    
    public AbstractController(AbstractModel minesweeper){
        this.minesweeper = minesweeper;
    }

    
    public void getInputs(int[] args) throws IllegalArgumentException{
        if((args[0] > 0) ) // all inputs value are positive and the percent is between 1 and 85
        {
            if(args[1] > 0)
            {
                if(args[2] > 0 && args[2] < 86)
                {
                    this.minesweeper.createGrid(args[0],args[1],args[2],args[3]);
                }
                else
                {
                    throw new IllegalArgumentException("Third arg is invalid (percent of mines) : between 1 and 85");
                }
                
            }
            else
            {
                throw new IllegalArgumentException("Second arg is invalid (number of rows) : must be positive");
            }
        }
        else
        {
            throw new IllegalArgumentException("First arg is invalid (number of columns) : must be positive");
        }
    }
    
    public boolean getGameState(){
        return this.minesweeper.getStateGame();
    }
    
    public boolean getDebugState(){
        return this.minesweeper.getDebugState();
    }
    
    public void setDebugState(boolean debug){
        this.minesweeper.setDebugState(debug);
    }
    
    public int getEnd(){
        return this.minesweeper.getEnd();
    }
    
    public void setCase(int i,int j, String action){
        this.grid_case_control[0] = i;
        this.grid_case_control[1] = j;
        this.action = action;
        control();
    }
    
    public void exit(){
        this.minesweeper.exitGame();
    }
    
    abstract void control();
    
        
}
