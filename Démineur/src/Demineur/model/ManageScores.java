package Demineur.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public class ManageScores implements Serializable{

    private List<Score> listScore;
    private final String fileName;
    private ObjectInputStream ios;
    private ObjectOutputStream oos;
    
    public ManageScores(String fileName){
        this.listScore = new LinkedList<>();
        this.fileName = fileName;
    }
        
    public List<Score> getScores(){
        this.readScores();
        this.sortList();
        return this.listScore;
    }
    public void addScore(int time,String username){
        this.readScores();
        this.listScore.add(new Score(username,time));
        this.writeScores();
    }
    
    public void sortList(){
        Collections.sort(this.listScore);
    }
        
    public void writeScores(){
        try{
            oos = new ObjectOutputStream(new FileOutputStream(fileName));
            oos.writeObject(this.listScore);
            System.out.println("Scores saved");
        }
        catch(IOException ioe){
            System.err.println("Input / output exception");
        }
        finally{
            try{
                if(this.oos != null)
                    this.oos.close();
            }
            catch(IOException ioe){
                System.err.println("Input/output exception");
            }
        }
        
    }
    
    public void readScores(){
        try{
            ios = new ObjectInputStream(new FileInputStream(fileName));
            this.listScore = (List<Score>)ios.readObject();
            System.out.println("Scores loaded");
        }
        catch(IOException ioe){
            System.err.println("Input/output exception");
        }
        catch(ClassNotFoundException cnfe){
            System.err.println("Class not found exception");
        }
        finally{
            try{
                if(this.ios != null)
                    this.ios.close();
            }
            catch(IOException ioe){
                System.err.println("Input/output exception");
            }
        }
    }
        
    
    public void dispScores(List<Score> list){
        for(Score score : list){
            System.out.println("val : "+score.getTime() + " user :"+score.getUsername());
        }
        
    }
       

}
