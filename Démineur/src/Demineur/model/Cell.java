package Demineur.model;
import java.util.ArrayList;
        
public class Cell
{
    CellStates m_state = CellStates.HIDDEN;
    ArrayList<Cell> m_notMineNeighbors = new ArrayList();
    int m_n = 0;
    int m_x;
    int m_y;

    public int getX() {
        return m_x;
    }

    public int getY() {
        return m_y;
    }
    
    public Cell()
    {        
    }
    public Cell(int i, int j, int n)
    {       
        m_x=i;
        m_y=j;
        m_n=n;
    }
    public Cell(CellStates cS)
    {
        m_state = cS;
    }
    public Cell(int n)
    {
        m_n = n;
    }

    public int getMines() {
        return m_n;
    }
    public Cell(CellStates cS, int n)
    {
        m_state = cS;
        m_n = n;
    }

    public ArrayList<Cell> getNeighbors() {
        return m_notMineNeighbors;
    }

    public CellStates getState() {
        return m_state;
    }

    public void setState(CellStates state) {
        this.m_state = state;
    }
}

