package Demineur.model;
import java.util.ArrayList;

public class MinesweeperModel extends AbstractModel{
        
    @Override
    public void createGrid(int width, int height, int minesPercent,int debug){
        this.height = height;
        this.width = width;
        this.minesPercent = minesPercent;
        this.debug = (debug == 1) ? true : false; // convert int to boolean
        this.nbMines = (height*width*minesPercent)/100; //number of mines in the grid
        boolean[][] mines = new boolean[height][width]; 
        
        for (int i = 0 ; i < height ; i++){
            for (int j = 0 ; j < width ; j++){           
                mines[i][j] = false;      
            }
        }

        while(this.nbMines > 0){    // loop to set the mines
            for (int i = 0 ; i < height ; i++){
                for (int j = 0 ; j < width ; j++){
                    if(mines[i][j] == false)
                    {
                        double perc = minesPercent;
                        perc/=100;
                        if(Math.random() <= (perc/100) && nbMines > 0)                
                        {
                            mines[i][j] = true;
                            this.nbMines--;
                        }
                        else{mines[i][j] = false;}
                    }                    
                }
            }
        }
        this.nbMines = (height*width*minesPercent)/100;
        initGrid(mines);
    }
    
    // method to set the mines in the grid and determine how many mines in his neighborhood
    private void initGrid(boolean [][] mines) 
    {
        grid = new ArrayList();
        for(int i = 0; i < this.height ; i++){
            ArrayList<Cell> I = new ArrayList();
            for(int j = 0 ; j < this.width ; j++){
               if(mines[i][j])
               {
                   I.add(j,new Cell(i,j,-1));     
               }
               else
               {
                   int n = 0;
                   //neightboor-cases in the top line
                    try{
                        if(mines[i-1][j-1]){n=n+1;}}
                    catch(Exception e){}
                    try{
                        if(mines[i][j-1]){n=n+1;}}
                    catch(Exception e){}
                    try{
                        if(mines[i+1][j-1]){n=n+1;}}
                    catch(Exception e){}
                    //neightboor-cases on the same line
                    try{
                        if(mines[i-1][j]){n=n+1;}}
                    catch(Exception e){}
                    try{
                        if(mines[i+1][j]){n=n+1;}}
                    catch(Exception e){}
                    //neightboor-cases in the bottom line
                    try{
                        if(mines[i+1][j+1]){n=n+1;}}
                    catch(Exception e){}
                    try{
                        if(mines[i-1][j+1]){n=n+1;}}
                    catch(Exception e){}
                    try{
                        if(mines[i][j+1]){n=n+1;}}
                    catch(Exception e){}
                    
                    I.add(j,new Cell(i,j,n));                                      
               }                
            }
            this.grid.add(i,I);
        }
        createNeighborsList();
    }
    
    private void createNeighborsList()
    {
       for(int i=0;i<this.height;i++){
           for(int j=0;j<this.width;j++){
                if (this.grid.get(i).get(j).getMines() == 0)
                {
                    for(int I = i-1 ; I <= i+1 ; I++){
                        for(int J = j-1 ; J <= j+1 ; J++){
                            if(J!=j || i!=I)
                            {
                                try{
                                    if( this.grid.get(I).get(J).getMines() != -1){
                                         this.grid.get(i).get(j).getNeighbors().add(this.grid.get(I).get(J));
                                }} catch(Exception e){}
                            }
                        }
                    }
                }
           }
       }
       notifyObserver(this.grid,this.still_in_game,this.nbMines); // notify the observer when the initialisation of the grid is finished
    }
    
    // method to mark the case according to the possible states defined in CellStates
    @Override
    public void markCase(int i, int j, CellStates mark){
        if (this.grid.get(i).get(j).getState() != CellStates.REVEALED )
        {
            if(mark == CellStates.MARKMINE && this.nbMines == 0)
            {
                 return;       
            }
            else
            {
                if(mark == CellStates.MARKMINE)
                {
                    this.nbMines--; // we decrement the number of mines if we mark a cell as a mine
                }
                if(this.grid.get(i).get(j).getState() == CellStates.MARKMINE && mark != CellStates.MARKMINE)
                {
                    this.nbMines++; // if we delete the mark on the cell mark as a mine, we increment the number of mines
                }
                if(mark == CellStates.MARKMINE && this.grid.get(i).get(j).getMines() == -1) 
                {
                    this.cellMarked++; // if the cell marked as a mine is really a mine, we increment the number of cell marked
                }
                if(this.grid.get(i).get(j).getMines() == -1 && mark != CellStates.MARKMINE)
                {
                    this.cellMarked--; // if the user deletes the markMine on a really mine, we decrement the number of cell marked
                }
                this.grid.get(i).get(j).setState(mark);
            }
        }
        winOrNot(); // check if we had win or not
        notifyObserver(this.grid,this.still_in_game, nbMines);
    }
    
    @Override
    public void revealCase(int i,int j){
        revealCaseRecursively(i,j);
        if(this.grid.get(i).get(j).getMines() != -1){
            winOrNot();
        }
        notifyObserver(this.grid,this.still_in_game,this.nbMines);
    }
   
    private void revealCaseRecursively(int i, int j){
        this.grid.get(i).get(j).setState(CellStates.REVEALED);
        this.cellRevealed++;
        if (this.grid.get(i).get(j).getMines() == -1)
        {
            end_game = -1;
            still_in_game = false;
        }
        if (this.grid.get(i).get(j).getMines() == 0)
        {// recursiv call when a cell isn't a neighboor of a mine or a mine
            for(int k = 0 ; k < this.grid.get(i).get(j).getNeighbors().size() ; k++){
                if(this.grid.get(i).get(j).getNeighbors().get(k).getState() != CellStates.REVEALED)
                {
                    if(this.grid.get(i).get(j).getNeighbors().get(k).getMines() == 0)
                    {                        
                        revealCaseRecursively(this.grid.get(i).get(j).getNeighbors().get(k).getX() , 
                                this.grid.get(i).get(j).getNeighbors().get(k).getY());
                    }
                    else
                    {
                        this.grid.get(i).get(j).getNeighbors().get(k).setState(CellStates.REVEALED); 
                        this.cellRevealed++;
                    }                    
                }       
            }
        }
    }
     
    
    private void winOrNot()
    {
        int totalCell = this.height*this.width; 
        if(this.cellMarked + this.cellRevealed == totalCell)
        {
            end_game = 1;
            still_in_game = false;
        }
       
    }
    
    @Override
    public void exitGame(){
        this.still_in_game = false;
        notifyObserver(this.grid,this.still_in_game,this.nbMines);
    }
    
    @Override
    public int getWidth(){
        return this.width;
    }
    
    @Override
    public int getHeight(){
        return this.height;
    }
    
    @Override
    public boolean getStateGame(){
        return this.still_in_game;
    }
    
    @Override
    public void setDebugState(boolean debug){
        this.debug = debug;
    }
    
    @Override
    public boolean getDebugState(){
        return this.debug;
    }
    
    @Override
    public int getEnd(){
        return this.end_game;
    }
    
    
}
