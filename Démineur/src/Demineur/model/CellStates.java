package Demineur.model;

public enum CellStates 
{
    HIDDEN,
    MARKSUSPECT,
    MARKMINE,
    REVEALED;   
}

