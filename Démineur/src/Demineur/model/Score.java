package Demineur.model;

import java.io.Serializable;

public class Score implements Serializable,Comparable<Score>{
    
    private String username;
    private Integer time;
    
    public Score(){
        this("null",-1);
    }
    
    public Score(String username, Integer time){
        this.username = username;
        this.time = time;
    }

    public String getUsername() {
        return username;
    }

    public Integer getTime() {
        return time;
    }
    
    @Override
    public int compareTo(Score other){
        return this.getTime().compareTo(other.getTime());
    }
    
    
    
}
