package Demineur.model;
import Demineur.observer.Observable;
import Demineur.observer.Observer;
import java.util.ArrayList;
import java.util.List;


public abstract class AbstractModel implements Observable {
    
    protected ArrayList<ArrayList<Cell>> grid;
    protected int height;
    protected int width;
    protected int minesPercent;
    protected boolean still_in_game = true;
    protected boolean debug ; 
    int cellMarked = 0;
    int cellRevealed = 0;
    protected int end_game = 0; // 0 still not lose, 1 : win, -1 : lose at last !
    protected int nbMines = 0;
    private List<Observer> listObserver = new ArrayList<Observer>();
     
    @Override
    public void addObserver(Observer obs){
        this.listObserver.add(obs);
    }
    
    @Override
    public void removeObserver(){
        this.listObserver = new ArrayList<Observer>();
    }
    
    @Override
    public void notifyObserver(ArrayList<ArrayList<Cell>> grid,boolean still_in_game,int nbMines){ 
        for(Observer obs : listObserver){
            obs.update(grid,still_in_game,nbMines);
        }
    }
    
    // now we list all the methods to implement in the concret model Demineur as asbtract method
   
    // method to initialise the grid according to the input datas
    public abstract void createGrid(int width,int height, int minesPercent,int debug);
    
    // reveal case(i,j)
    public abstract void revealCase(int i, int j);
    
    //mark case(i,j) as mine or unknown 
    public abstract void markCase(int i,int j,CellStates mark);
       
    // end of the game
    public abstract void exitGame();
    
    public abstract int getHeight();
    
    public abstract int getWidth();
    
    public abstract boolean getStateGame();
    
    public abstract boolean getDebugState();
    
    public abstract void setDebugState(boolean debug);
    
    public abstract int getEnd();
    
}
