package Demineur.view;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Demineur.controller.AbstractController;


public class ActionDefinedParameters implements ActionListener{
    private final int rows; 
    private final int columns;
    private final int percentMines;
    private final int debug ;
    private final AbstractController controller;
    
    public ActionDefinedParameters(int rows,int columns,int percentMines,int debug,AbstractController controller){
        this.rows = rows;
        this.columns = columns;
        this.percentMines = percentMines;
        this.debug = debug;
        this.controller = controller;         
    }
    
    @Override
    public void actionPerformed(ActionEvent ae){
        int[] args = new int[4];
        args[0] = this.columns;
        args[1] = this.rows;
        args[2] = this.percentMines;
        args[3] = this.debug ;
        controller.getInputs(args);
    }
}
