package Demineur.view;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import java.awt.BorderLayout;
import Demineur.observer.Observer;
import Demineur.controller.AbstractController;
import Demineur.model.Cell;
import java.util.ArrayList;
import java.awt.event.KeyEvent;
import javax.swing.KeyStroke;
import java.awt.event.InputEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.Toolkit;
import java.awt.Dimension;


public class GraphicalView extends JFrame implements Observer {
    private final AbstractController controller;
    private StateBar stateB = new StateBar();
    private JPanel jP = new JPanel();
    private JPanel bigPanel;
    private final JMenuBar menuBar ;
    private final JMenu gameMenu ;
    private final JMenu newGame;
    private final JMenuItem quitGame,beginner,intermediate,expert,custom;
    private boolean reLocation = true;
    
    
    public GraphicalView(AbstractController controllerEntry)
    {        
        super("Minesweeper game");
        
        this.addWindowListener(new WindowListener(){
            @Override
            public void windowOpened(WindowEvent e){}
            @Override
            public void windowClosing(WindowEvent e){}
            @Override
            public void windowClosed(WindowEvent e){System.exit(0);}
            @Override
            public void windowIconified(WindowEvent e){}
            @Override
            public void windowDeiconified(WindowEvent e){}
            @Override
            public void windowActivated(WindowEvent e){}
            @Override
            public void windowDeactivated(WindowEvent e){}
        });
        this.controller = controllerEntry;
        this.bigPanel = new JPanel();
        this.bigPanel.setLayout(new BorderLayout());
        this.bigPanel.add(this.jP);
        this.bigPanel.add(this.stateB, BorderLayout.SOUTH);
        this.add(bigPanel);
        this.setSize(300,400);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        // initialisation of our items
        menuBar = new JMenuBar();
        gameMenu = new JMenu("Game");
        gameMenu.setMnemonic(KeyEvent.VK_G);
        newGame = new JMenu("New");
        newGame.setMnemonic(KeyEvent.VK_N);
        quitGame = new JMenuItem("Quit",KeyEvent.VK_Q);
        quitGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,InputEvent.CTRL_MASK));
        quitGame.setMnemonic(KeyEvent.VK_Q);
        quitGame.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae){
                System.exit(1);
            }
        });
        beginner = new JMenuItem("Beginner");
        beginner.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B,InputEvent.CTRL_MASK));
        beginner.setMnemonic(KeyEvent.VK_B);
        beginner.addActionListener(new ActionDefinedParameters(9,9,9,0,controller));
        intermediate = new JMenuItem("Intermediate");
        intermediate.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I,InputEvent.CTRL_MASK));
        intermediate.setMnemonic(KeyEvent.VK_I);
        intermediate.addActionListener(new ActionDefinedParameters(16,16,16,0,controller));
        expert = new JMenuItem("Expert");
        expert.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,InputEvent.CTRL_MASK));
        expert.setMnemonic(KeyEvent.VK_E);
        expert.addActionListener(new ActionDefinedParameters(16,30,21,0,controller));
        custom = new JMenuItem("Custom");
        custom.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,InputEvent.CTRL_MASK));
        custom.setMnemonic(KeyEvent.VK_C);
        custom.addActionListener(new ActionListener(){
           @Override
           public void actionPerformed(ActionEvent ae){
               MenuJBar jbar = new MenuJBar("Custom parameters",controller);
           }
        });
        // let's add our items to the corresponding menus
        newGame.add(beginner);
        newGame.add(intermediate);
        newGame.add(expert);
        newGame.add(custom);
        gameMenu.add(newGame);
        gameMenu.add(quitGame);
        menuBar.add(gameMenu);
        // we add our JMenuBar to the frame by the method setJMenuBar
        this.setJMenuBar(menuBar);
        // setting properties of the JFrame
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);  
        this.setResizable(false);
        this.setAlwaysOnTop(true);
        
    }
    
    @Override
    public void update(ArrayList<ArrayList<Cell>> grid,boolean still_in_game,int nbMines)
    {
        if(this.controller.getEnd() == -1){
            this.stateB = new StateBar("Try again, you lose");
            controller.setDebugState(true); // set debug to true to display the mine at the end of the game
        }
        if(this.controller.getEnd() == 1){
            this.stateB = new StateBar("You win");
            controller.setDebugState(true); // set debug to true to display the mine at the end of the game
        }
        if(this.controller.getEnd() == 0){
             this.stateB = new StateBar("Remaining mines " + nbMines);
        }        
        
        this.jP = new GraphicalGridView(grid, controller.getDebugState(),controller.getGameState(), this.controller);
        this.remove(bigPanel);
        this.bigPanel = new JPanel();
        this.bigPanel.setLayout(new BorderLayout());
        bigPanel.add(jP);
        bigPanel.add(this.stateB, BorderLayout.SOUTH);
        this.add(bigPanel);        
        this.repaint();        
        this.pack();
        
        if(reLocation){Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        reLocation = false;
        }
        this.show();   
        if(this.controller.getEnd() == -1){
            JOptionPane.showMessageDialog(this,"Try again, you lose !");
        }
        if(this.controller.getEnd() == 1){
            JOptionPane.showMessageDialog(this,"Congratulations, you win !");
        } 
        if(!still_in_game){
            this.dispose();
        }
    }
}