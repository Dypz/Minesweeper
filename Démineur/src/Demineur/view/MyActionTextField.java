package Demineur.view;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class MyActionTextField implements ActionListener{
    private final int min;
    private final int max;
    private final Integer defaultValue;
    private final ItemsSlider item;
    private final ItemsSlider toInteractWith;
    private final ItemsSlider partner;

    public MyActionTextField(int min, int max,Integer defaultValue,ItemsSlider item,ItemsSlider partner,ItemsSlider toInteractWith){
        this.min = min;
        this.max = max;
        this.defaultValue = defaultValue;
        this.item = item;
        this.partner = partner;
        this.toInteractWith = toInteractWith;
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        String stringTextField = e.getActionCommand();
        try{
            Integer valueTextField = Integer.parseInt(stringTextField);
            if(valueTextField >= min && valueTextField <= max){
                item.getSlider().setValue(valueTextField);
            }
        }
        catch(NumberFormatException nfe){
            //JOptionPane.showMessageDialog(,"Invalid value : rows : [9,24] / columns[9,30]");
            item.getTextField().setText(defaultValue.toString()); // we set a random value which respect the instructions
            item.getSlider().setValue(defaultValue);
        }
        finally{
            if(toInteractWith != null && partner != null){
                Double maximum = 0.85 * item.getSlider().getValue() * partner.getSlider().getValue();
                toInteractWith.getSlider().setMaximum(maximum.intValue());
                toInteractWith.getSlider().setMajorTickSpacing((maximum.intValue()-10)/2);
                toInteractWith.getSlider().setMinorTickSpacing((maximum.intValue()-10)/4);
            }
        }
    }
}
