package Demineur.view;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class MyActionSlider implements ChangeListener{
    private final ItemsSlider item;
    private final ItemsSlider toInteractWith;
    private final ItemsSlider partner;

    public MyActionSlider(ItemsSlider item,ItemsSlider partner,ItemsSlider toInteractWith){
        this.item = item;
        this.partner = partner;
        this.toInteractWith = toInteractWith;
    }
    @Override
    public void stateChanged(ChangeEvent e){ 
        Integer valueSlider = item.getSlider().getValue();
        item.getTextField().setText(valueSlider.toString());// we actualise the value of the JTextField
        if(toInteractWith != null && partner != null){
            Double maximum = 0.85 * item.getSlider().getValue() * partner.getSlider().getValue();
            toInteractWith.getSlider().setMaximum(maximum.intValue());
            toInteractWith.getSlider().setLabelTable(null);
            toInteractWith.getSlider().setMajorTickSpacing((maximum.intValue()-10)/4);
            toInteractWith.getSlider().setMinorTickSpacing((maximum.intValue()-10)/8);
        }
    }
    
}
