package Demineur.view;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import Demineur.controller.AbstractController;
import javax.swing.JFrame;


public class ActionOnConfirm implements ActionListener{ 
    
    private final ButtonGroup bg;
    private final CustomGamePanel customGame;
    private final AbstractController controller;
    private final JFrame frame;
    
    public ActionOnConfirm(ButtonGroup bg, CustomGamePanel customGame,AbstractController controller,JFrame frame){
        this.bg = bg;
        this.customGame = customGame;
        this.controller = controller;
        this.frame = frame;
    }
    
    @Override
    public void actionPerformed(ActionEvent ae){
        int[] args = new int[4];
        args[3] = 0 ; // debugging mode not activate
        switch(bg.getSelection().getActionCommand())
        {
            case "Beginner":
                args[0] = 9;
                args[1] = 9;
                args[2] = 9;
                this.controller.getInputs(args);
                break;
            case "Intermediate":
                args[0] = 16;
                args[1] = 16;
                args[2] = 16;
                this.controller.getInputs(args);
                break;
            case "Expert":
                args[0] = 30;
                args[1] = 16;
                args[2] = 21;
                this.controller.getInputs(args);
                break;
            case "Custom":
                args[0] = customGame.getItemSlider("columns").getSlider().getValue();
                args[1] = customGame.getItemSlider("rows").getSlider().getValue();
                int nbMines = customGame.getItemSlider("mines").getSlider().getValue();
                Double percentMines = ((double)nbMines / (double)(args[0]*args[1]))*100;
                args[2] = percentMines.intValue();
                this.controller.getInputs(args);
                break;
        }
        this.frame.dispose();
    }
    
}
