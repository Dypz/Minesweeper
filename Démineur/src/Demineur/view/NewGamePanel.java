package Demineur.view;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import java.awt.Dimension;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import Demineur.controller.AbstractController;
import javax.swing.JFrame;

public class NewGamePanel extends JPanel{

    private final MyRadioButton[] items = new MyRadioButton[4];
    private final JPanel[] panels = new JPanel[4];
    private final ButtonGroup bg;
    private final JPanel container;
    private final JButton confirm;
    
    public NewGamePanel(AbstractController controller,JFrame frame){
        JPanel levelPanel = new JPanel();
        levelPanel.add(new JLabel("Select a level : "));
        levelPanel.setLayout(new BoxLayout(levelPanel,BoxLayout.X_AXIS));
        items[0] = new MyRadioButton(new ImageIcon(getClass().getResource("beginner.jpg")),"Beginner : 10 mines in a 9*9 field","Beginner");
        items[1] = new MyRadioButton(new ImageIcon(getClass().getResource("intermediate.jpg")),"Intermediate : 40 mines in a 16*16 field","Intermediate");
        items[2] = new MyRadioButton(new ImageIcon(getClass().getResource("expert.jpg")),"Expert : 99 mines in a 16*30 field","Expert");
        items[3] = new MyRadioButton(new ImageIcon(getClass().getResource("custom.jpg")),"Custom : ","Custom");
        items[3].getRadioButton().setSelected(true); // initially the custom parameter is selected
        bg = new ButtonGroup();
        container = new JPanel();
        container.setLayout(new BoxLayout(container,BoxLayout.Y_AXIS));
        container.add(levelPanel);
        for(int i=0;i<panels.length;i++){
            panels[i] = new JPanel();
            panels[i].add(items[i]);
            bg.add(items[i].getRadioButton());
            panels[i].setLayout(new BoxLayout(panels[i],BoxLayout.X_AXIS));
            container.add(panels[i]);
        }
        this.add(container);
        CustomGamePanel customGame = new CustomGamePanel();
        this.add(customGame);
        confirm = new JButton("Confirm choice");
        confirm.addActionListener(new ActionOnConfirm(bg,customGame,controller,frame));
        this.add(confirm);
        this.setPreferredSize(new Dimension(500,350));
        this.setMinimumSize(this.getPreferredSize());
        this.setMaximumSize(this.getPreferredSize());
    }
        
}
