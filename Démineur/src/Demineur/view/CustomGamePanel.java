package Demineur.view;
import javax.swing.JPanel;
import javax.swing.BoxLayout;


public class CustomGamePanel extends JPanel{
    private final ItemsSlider rows;
    private final ItemsSlider columns;
    private final ItemsSlider mines;
        
    public CustomGamePanel(){
        super();
        this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
        Integer initRows = 9;
        Integer initColumns = 19;
        Double maxMines = 0.85*initRows*initColumns;
        rows = new ItemsSlider("Rows",9,24,1,2,9);
        columns = new ItemsSlider("Columns",9,30,2,4,19);
        mines = new ItemsSlider("Mines",10,maxMines.intValue(),13,26,76);
        rows.getSlider().addChangeListener(new MyActionSlider(rows,columns,mines));
        columns.getSlider().addChangeListener(new MyActionSlider(columns,rows,mines));
        mines.getSlider().addChangeListener(new MyActionSlider(mines,null,null));
        rows.getTextField().addActionListener(new MyActionTextField(9,24,9,rows,columns,mines));
        columns.getTextField().addActionListener(new MyActionTextField(9,30,19,columns,rows,mines));
        mines.getTextField().addActionListener(new MyActionTextField(10,maxMines.intValue(),76,mines,null,null));
        this.add(rows);
        this.add(columns);
        this.add(mines);
    }
    
    public ItemsSlider getItemSlider(String item){
        switch(item)
        {
            case "rows":
                return this.rows;
            case "columns":
                return this.columns;
            case "mines":
                return this.mines;
            default:
                return null;
        }
    }
        
}
