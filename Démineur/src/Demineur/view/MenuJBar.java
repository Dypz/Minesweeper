package Demineur.view;
import javax.swing.JFrame;
import Demineur.controller.AbstractController;


public class MenuJBar extends JFrame{
    private final NewGamePanel gamePanel;
    private final AbstractController controller;

    public MenuJBar(String title,AbstractController controller){
        super(title);
        this.controller = controller;
        gamePanel = new NewGamePanel(this.controller,this);
        this.add(gamePanel);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setAlwaysOnTop(true);
        this.setSize(400,350);
        this.setVisible(true);
        
    }
    
}
