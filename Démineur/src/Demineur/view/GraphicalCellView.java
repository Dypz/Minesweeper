package Demineur.view;
import javax.swing.JButton;
import Demineur.model.Cell;
import Demineur.model.CellStates;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.ImageIcon;
import Demineur.controller.AbstractController;
import java.awt.Dimension;
import javax.swing.SwingConstants;

public class GraphicalCellView extends JButton implements MouseListener
{
    private final ImageIcon mine = new ImageIcon(getClass().getResource("mine.png"));
    private final ImageIcon flag_mine = new ImageIcon(getClass().getResource("flag.png"));
    private final ImageIcon unknown = new ImageIcon(getClass().getResource("unknown.gif"));
    private boolean enable = true;
    private AbstractController controller;
    private int x,y;
    private GraphicalGridView grid;
    private Cell myself; 
 
    @Override
    public void mousePressed(MouseEvent e) 
	{
	    if(e.getButton() == MouseEvent.BUTTON1)
	    {
              this.controller.setCase(x, y, "REVEAL");
	    }	    
	    else if(e.getButton() == MouseEvent.BUTTON3)
	    {
                String action = "MARK_AS_MINE";
                switch(myself.getState())
                {
                    case MARKSUSPECT : action = "DELETE_MARK";break;
                    case HIDDEN : action = "MARK_AS_MINE";break;
                    case MARKMINE : action = "MARK_AS_UNKNOWN";                    
                }
                this.controller.setCase(x, y, action);
	    }
	}
	
	public void mouseReleased(MouseEvent e)
	{}
	public void mouseEntered(MouseEvent e) 
	{}
	public void mouseExited(MouseEvent e) 
	{}
	public void mouseClicked(MouseEvent e) 
	{}
    
    
    public GraphicalCellView()
    {
        super();  
    }
    
    public GraphicalCellView(Cell c, boolean debug,boolean in_game,int x, int y, AbstractController controller)
    {
        super();
        this.x=x;this.y=y;
        this.myself = c;
        this.setFocusPainted(false);
        this.controller = controller;
        this.setText(" ");
        
        if(debug)
        {
            if (c.getState() == CellStates.HIDDEN && c.getMines() == -1)
            {
                super.setIcon(this.mine);
            }
            if(c.getMines() == -1 && in_game == false){
                super.setIcon(this.mine);
            }
        }
        if (c.getState()== CellStates.MARKMINE && in_game)
        {
            super.setIcon(this.flag_mine);
        }
        if (c.getState()== CellStates.MARKSUSPECT && in_game)
        {
            super.setIcon(this.unknown);
        }
        if (c.getState()==CellStates.REVEALED)
        {
            super.setContentAreaFilled(false);
            this.enable = false;
            if (c.getMines() == -1)
            {
                super.setIcon(this.mine);
            }   
            if (c.getMines() > 0)
            {
                if(c.getMines() == 1){super.setText("<html><font color = green>"+String.valueOf(c.getMines())+"</font></html>");}
                if(c.getMines() == 2){super.setText("<html><font color = orange>"+String.valueOf(c.getMines())+"</font></html>");}
                if(c.getMines() > 2){super.setText("<html><font color = red>"+String.valueOf(c.getMines())+"</font></html>");}
             }
        }
        if(enable){this.addMouseListener(this);}         
        this.setPreferredSize(new Dimension(40, 30));
        this.setHorizontalTextPosition(SwingConstants.CENTER);
    }
    
}