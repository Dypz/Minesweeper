package Demineur.view;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JSlider;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Dimension;


public class ItemsSlider extends JPanel{
    private final JSlider slider;
    private final JTextField textField;
    public ItemsSlider(String label,int min, int max,int minorTickSpacing, int majorTickSpacing,Integer defaultValue){
        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        this.slider = new JSlider(min,max,defaultValue);
        slider.setMinorTickSpacing(minorTickSpacing);
        slider.setMajorTickSpacing(majorTickSpacing);
        slider.setPaintTicks(true); // display the ticks
        slider.setPaintLabels(true); // display the label values
        this.add(new JLabel(label));
        this.add(slider);
        this.textField = new JTextField(defaultValue.toString());
        textField.setPreferredSize(new Dimension(45,20));
        textField.setMinimumSize(textField.getPreferredSize());
        textField.setMaximumSize(textField.getPreferredSize());
        this.add(textField);
    }
    
    public JTextField getTextField(){
        return this.textField;
    }
    
    public JSlider getSlider(){
        return this.slider;
    }
    
}
