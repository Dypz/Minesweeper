package Demineur.view;
import Demineur.observer.Observer;
import Demineur.controller.AbstractController;
import Demineur.model.Cell;
import Demineur.model.CellStates;
import java.util.ArrayList;
import java.util.Scanner;

public class Minesweeper implements Observer{ // extends JFrame quand on utilisera les swing pour l'affichage
    
    private AbstractController controller;
    //Scanner scan = new Scanner(System.in);
    
    public Minesweeper(AbstractController controller){
        this.controller = controller;
    }
    
    /*public void sendArgs(){
        int argues[] = getArgs();
        // we send the datas to the controller to check them
        try{
            controller.getInputs(argues);
        }
        catch(IllegalArgumentException ie)
        {
            System.err.println(ie.getMessage());
            System.exit(1);
        }
    }
    
    private int[] getArgs(){
        int[] argues = new int[4]; // 0-> nb_cols; 1-> nb_rows, 2 -> percent of mines, 3 -> debug mode
        
        System.out.println("Welcome in a minesweeper game ! ");
        System.out.println("Choose the dimensions of your minesweeper : ( nb_cols then ENTER_KEY, nb_rows then ENTER_KEY )");
        try{
            argues[0] = scan.nextInt();
            argues[1] = scan.nextInt();
        }
        catch(java.util.InputMismatchException e){
            System.err.println("First(number of columns) and second(number of rows) parameters must be integers");
            System.exit(1);
        }
        System.out.println("Choose the percent of mines in your minesweeper");
        try{
            argues[2] = scan.nextInt();
        }
        catch(java.util.InputMismatchException e){
            System.err.println("Third parameter(mines percent) must be an integer");
            System.exit(1);
        }
        System.out.println("Do you want to activate the debug mode ? (y/n) ");
        try{
            String debug_mode = scan.next();
            switch(debug_mode)
            {
                case "y":
                    argues[3] = 1; // 1 if we want to use the debug mode
                    break;
                case "n":
                    argues[3] = 0; // 0 if we don't want to use the debug mode
                    break;
                default :
                    throw new IllegalArgumentException("Fourth parameter(debugging mode) must be equal to 'y' or 'n'" );
            }
        }
        catch(IllegalArgumentException e){
            System.err.println(e.getMessage());
            System.exit(1);
        }
        return argues;
    }*/
    
    public void getCommands(){
        Scanner scan = new Scanner(System.in);
        String user_request;
        String[] tokens;
        //scan.nextLine(); // clear buffer of the scanner
        while(this.controller.getGameState())
        {
            if(scan.hasNext())
            {
                user_request = scan.nextLine();
                tokens = user_request.split(" ");
                if(tokens[0].equals("q") && tokens.length == 1){
                    this.controller.exit();
                    System.out.println("Game quit by user");
                    break;
                }
                else if(tokens[0].equals("d") && tokens.length == 3){
                    this.controller.setCase(Integer.parseInt(tokens[1]),Integer.parseInt(tokens[2]), "REVEAL");
                }
                else if(tokens[0].equals("m") && tokens.length == 4){
                    switch(tokens[3])
                    {
                        case "x":
                            this.controller.setCase(Integer.parseInt(tokens[1]),Integer.parseInt(tokens[2]),"MARK_AS_MINE");
                            break;
                        case "?":
                            this.controller.setCase(Integer.parseInt(tokens[1]),Integer.parseInt(tokens[2]),"MARK_AS_UNKNOWN");
                            break;
                        case "#":
                           this.controller.setCase(Integer.parseInt(tokens[1]),Integer.parseInt(tokens[2]),"DELETE_MARK");
                            break;
                        default:
                           System.out.println("Unknown request");
                    }
                }
                else
                {
                    System.out.println("Unknown request");
                }
            }
        }
    }
    
   
    public void printGrid(ArrayList<ArrayList<Cell>> grid)
    {    
        for(int i=0;i<grid.size();i++)
        {
            for(int j=0;j<grid.get(i).size();j++)
            {
                if(grid.get(i).get(j).getState() == CellStates.REVEALED && grid.get(i).get(j).getMines() != -1)
                {
                    System.out.print (grid.get(i).get(j).getMines() + " ");
                }
                else if(grid.get(i).get(j).getState() == CellStates.MARKSUSPECT){
                    System.out.printf("? ");
                }
                else if(grid.get(i).get(j).getState() == CellStates.MARKMINE){
                    System.out.printf("! ");
                }
                else
                {
                    if(grid.get(i).get(j).getMines()== -1 && this.controller.getDebugState() && (this.controller.getEnd() == 0) )
                    {
                        String a = "\033[1m# \033[0m";
                        System.out.printf(a);
                    }
                    else if (grid.get(i).get(j).getMines()== -1 && (this.controller.getEnd() != 0)){
                        System.out.printf("x ");
                    }
                    else{System.out.printf("# ");}                    
                }
            }            
            System.out.println();
        }System.out.println();
        if(this.controller.getEnd() == 1){
            System.out.println("Congratulations, you win !");
        }
        if(this.controller.getEnd()== -1){
            System.out.println("Try again, you lose !");
        }
    }
    
    //Implémentation pattern observer
    @Override
    public void update(ArrayList<ArrayList<Cell>> grid,boolean still_in_game,int nbMines){
        if(still_in_game == false && this.controller.getEnd() == 0)
        {
            System.exit(1);            
        }
        printGrid(grid);
    }
    
}