package Demineur.view;
import Demineur.controller.AbstractController;
import Demineur.model.Cell;
import java.awt.*;
import java.util.ArrayList;
import javax.swing.JPanel;

public class GraphicalGridView extends JPanel
{
    private AbstractController controller;
    
    public GraphicalGridView()
    {
        super();
    }
    
    public GraphicalGridView(ArrayList<ArrayList<Cell>> grid, boolean debug,boolean in_game, AbstractController controller)
    {   
        super();
        this.controller = controller;
        int x = grid.size();
        int y = grid.get(0).size();        
        this.setLayout(new GridLayout(x, y, 0, 0));
        for(int i = 0; i < x;i++){for(int j = 0; j < y; j++)
        {
            GraphicalCellView cell = new GraphicalCellView(grid.get(i).get(j), debug,in_game,i,j, this.controller);
            this.add(cell);
        }}
    }
    
}