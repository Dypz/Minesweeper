package Demineur.view;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;

public class StateBar extends JPanel
{
    public StateBar()
    {
        super();
        this.setLayout(new BorderLayout());
        JLabel label = new JLabel();
        this.add(label, BorderLayout.CENTER);
    }
    public StateBar(String s)
    {
        super();
        this.setLayout(new BorderLayout());
        JLabel label = new JLabel(s);
        this.add(label, BorderLayout.CENTER);
    }
    
}