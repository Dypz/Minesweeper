package Demineur.view;
import Demineur.model.*;
import Demineur.controller.*;



public class Main {

    public static void main(String[] args) {
        // We instance our model
        AbstractModel minesweeper = new MinesweeperModel();
        // Creation of the controller
        AbstractController minesweeperController = new MinesweeperController(minesweeper);
        // Creation of our view
        Minesweeper view_minesweeper = new Minesweeper(minesweeperController);
        GraphicalView graphical_view_minesweeper = new GraphicalView(minesweeperController);
        // Add our view as an observer of our model
        minesweeper.addObserver(view_minesweeper);
        minesweeper.addObserver(graphical_view_minesweeper);
        //view_minesweeper.sendArgs();
        // possible to get data from the console, when using swing the evenements will happened in the view Minesweeper
        while(minesweeperController.getGameState()){
            view_minesweeper.getCommands();
        }
    }
    
}
