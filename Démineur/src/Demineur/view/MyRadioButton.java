package Demineur.view;
import javax.swing.JRadioButton;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;


public class MyRadioButton extends JPanel{
    private final JRadioButton button;
    private final JLabel jlabel;
    
    public MyRadioButton(ImageIcon img, String label,String actionCommand){
        super();
        button = new JRadioButton();
        button.setActionCommand(actionCommand);
        this.add(button);
        jlabel = new JLabel(label);
        jlabel.setIcon(img);
        jlabel.setLabelFor(button);
        this.add(jlabel);
    }
    
    public JRadioButton getRadioButton(){
        return this.button;
    }
}
